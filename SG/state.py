# Author Darrell Best: bbest@g.clemson.edu
# Date 10/06/2014
#
# Singleton State class holds global variables

class State:
  
    def __init__(self):
        self.mouse_velocity_coord = [0, 0]
        self.mouse_coord = [0, 0]
        self.window_res = [0, 0]
        self.screen = 55
        self.viewing_dist = 61
        self.butter_worth = 0
        self.dfwidth = 10
        self.dfdegree = 2
        self.dfo = 1
        self.threshold =20
        self.fixating = 0
        self.refs = list()
        self.pin = ""

    def set_mouse_velocity_coord_x(self, x):
        self.mouse_velocity_coord[0] = x

    def set_mouse_velocity_coord_y(self, y):
        self.mouse_velocity_coord[1] = y

    def set_mouse_coord_x(self, x):
        self.mouse_coord[0] = x

    def set_mouse_coord_y(self, y):
        self.mouse_coord[1] = y

    def set_window_width(self, w):
        self.window_res[0] = w

    def set_window_height(self, h):
        self.window_res[1] = h
    
    def set_screen(self, s):
        self.screen = s

    def set_viewing_dist(self, d):
        self.viewing_dist = d

    def set_butter_worth(self, flag):
        self.butter_worth = flag

    def set_dfwidth(self,d):
        self.dfwidth = d

    def set_dfdegree(self,d):
        self.dfdegree = d

    def set_dfo(self,d):
        self.dfo = d

    def set_threshold(self,T):
        self.threshold = T

    def set_fixating(self,f):
        self.fixating = f

    def get_mouse_velocity_coord_x(self):
        return self.mouse_velocity_coord[0]
    
    def get_mouse_velocity_coord_y(self):
        return self.mouse_velocity_coord[1]
    
    def get_mouse_coord_x(self):
        return self.mouse_coord[0]
    
    def get_mouse_coord_y(self):
        return self.mouse_coord[1]
    
    def get_window_width(self):
        return self.window_res[0]
    
    def get_window_height(self):
        return self.window_res[1]

    def get_screen(self):
        return self.screen
    
    def get_viewing_dist(self):
        return self.viewing_dist
    
    def get_butter_worth(self):
        return self.butter_worth
    
    def get_dfwidth(self):
        return self.dfwidth

    def get_dfdegree(self):
        return self.dfdegree

    def get_dfo(self):
        return self.dfo

    def get_threshold(self):
        return self.threshold

    def get_fixating(self):
        return self.fixating

    def get_pin(self, rotary):
        if len(self.refs) > 0:
            self.pin = rotary.get_pin(self.refs, \
                self.window_res[0], self.window_res[1])
        return self.pin
    
    def get_refs(self):
        return self.refs

    def add_to_pin(self, i):
        self.append(i)
    
    def add_to_refs(self, ref):
        self.refs.append(ref)

    def reset_pin(self):
        self.pin = ""

    def reset_refs(self):
        self.refs = list()


