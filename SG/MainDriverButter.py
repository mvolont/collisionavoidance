#!/usr/bin/env python
'''
Created on Sep 19, 2014

@author: Darrell Best, bbest@clemson.edu
'''

import sys, math

from rotary import Rotary
from state import State
from point import Point
from bw import Butterworth as Bw
from sg import SG

from itertools import tee, islice, chain, izip


from math import *
#from OpenGL.GL import *
#from OpenGL.GLUT import *
#from OpenGL.GLU import *

import csv

# globals
global state, rotary, bw_filter_x, bw_filter_y, sg_filter_x, sg_filter_y



def display():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glLoadIdentity()
    ww = state.get_window_width()
    wh = state.get_window_height()
    refresh2d(ww, wh)
    
    rotary = Rotary()
        
    # draw outer circle
    glColor3f(0.3, 0.3, 0.3)
    draw_circle(ww / 2, wh / 2, (wh * .99) / 2, 100, GL_TRIANGLE_FAN)
    
    # draw outer circle outline
    glColor3f(1.0, 1.0, 1.0)
    draw_circle(ww / 2, wh / 2, (wh * .99) / 2, 100, GL_LINE_LOOP)
    
    # draw center pad
    glColor3f(0.8, 0.8, 0.8)
    draw_circle(ww / 2, wh / 2, (wh * .41) / 2, 100, GL_LINE_LOOP)
    
    # save center pad
    rotary.addPad((ww / 2), (wh / 2), ((wh * .41) / 2), ("c"))
    
    # draw center circle
    glColor3f(1.0, 1.0, 1.0)
    draw_circle(ww / 2, wh / 2, (wh * .10) / 2, 100, GL_TRIANGLE_FAN)
    
    # draw 10 pads, 0 - 9
    glColor3f(1.0, 1.0, 1.0)
    draw_pads(10, rotary)
    
    # draw mouse location
    detect_pad_col(rotary)
#   draw_rect(0, 0, 0, 0)
        
    glutSwapBuffers()

def detect_pad_col(rotary):
    mx = state.get_mouse_coord_x()
    my = state.get_mouse_coord_y()
    ww = state.get_window_width()
    wh = state.get_window_height()
    rect_dim = 10 if not state.get_fixating() else 20
    if rotary.getPad(mx, my, 0) != None:
        glColor3f(0.0, 1.0, 0.0)
        draw_rect(mx, wh - my, rect_dim, rect_dim)
        state.add_to_refs(rotary.getPad(mx, my, 0).getReff())
    else:
        glColor3f(1.0, 0.0, 0.0)
        draw_rect(mx, wh - my, rect_dim, rect_dim)
    pin = state.get_pin(rotary)
    glColor3f(0.0, 0.0, 0.0)
    for i in range(len(pin)):
        draw_character("****", ((ww / 2) - 15 + ((i - 1) * 15)), (wh / 2) - 10, i)
    if len(pin) == 4:
        print pin
        if pin == "1234":
            glColor3f(0.0, 1.0, 0.0)
            draw_circle(ww / 2, wh / 2, (wh * .10) / 2, 100, GL_TRIANGLE_FAN)
            print "You entered your pin correctly!"
            sys.exit()
        else:
            glColor3f(1.0, 0.0, 0.0)
            draw_circle(ww / 2, wh / 2, (wh * .10) / 2, 100, GL_TRIANGLE_FAN)
            state.reset_pin()
            state.reset_refs()

def draw_rect(x, y, width, height):
    glBegin(GL_QUADS)
    glVertex2f(x - (width / 2), y - (height / 2))
    glVertex2f(x + (width / 2), y - (height / 2))
    glVertex2f(x + (width / 2), y + (height / 2))
    glVertex2f(x - (width / 2), y + (height / 2))
    glEnd()

def draw_circle(cx, cy, r, num_segments, type):
    theta = 2 * 3.1415926 / float(num_segments)
    # tangential factor
    tanf = tan(theta)
    # radial factor
    radf = cos(theta)
    # start x at angle 0
    x = r
    y = 0
    glBegin(type)
    i = 0
    for i in range(num_segments):
        glVertex2f(x + cx, y + cy)
        tx = -y
        ty = x
        x = x + tx * tanf
        y = y + ty * tanf
        x = x * radf
        y = y * radf
    glEnd();

def draw_character(char_string, x, y, i):
    chars = char_string
    glRasterPos2f(x, y)
    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, ord(chars[i]))

def draw_pads(num, rotary):
    ww = state.get_window_width()
    wh = state.get_window_height()
    for i in range(num):
        r = i * (360 / num)
        # x = cx + r * cos(a)
        x = (ww / 2) + ((wh * .35) * cos(radians(r)))
        y = (wh / 2) + ((wh * .35) * sin(radians(r)))
        # draw pads with radius of 16% of the screen height
        draw_circle(x, y, (wh * .16) / 2, 100, GL_LINE_LOOP)
        # save pad
        rotary.addPad(x, y, (wh * .16) / 2, str(9 - i))
        # draw numbers in those pads
        draw_character('9012345678', x - 6, y - 6, i);

def reshape(width, height):
    state.set_window_width(glutGet(GLUT_WINDOW_WIDTH))
    state.set_window_height(glutGet(GLUT_WINDOW_HEIGHT))
    glViewport(0, 0, width, height)

def keyboard(key, x, y):
    if key == '\033':
        sys.exit( )
    # enter and exit butterworth filter mode
    if key == 'b':
        print state.get_butter_worth()
        if state.get_butter_worth() == 0:
            state.set_butter_worth(1)
        else:
            state.set_butter_worth(0)
    # reset refs and pin
    if key == 'r':
        state.reset_pin()
        state.reset_refs()
    # print pin
    if key == 'p':
        print state.get_pin(rotary)

        
  
    
def previous_and_next(some_iterable):
    prevs, items, nexts = tee(some_iterable, 3)
    prevs = chain([None], prevs)
    nexts = chain(islice(nexts, 1, None), [None])
    return izip(prevs, items, nexts)    
        
        
def mouse_stream():
    out=open("201605_ButterFix.csv","wb")
    output=csv.writer(out)
    
    outD=open("201605__ButterDur.csv","wb")
    outputD=csv.writer(outD)
    
    customFixeta = []
    durationList = []
    with open('201605.csv', 'rU') as infile:
      # read the file as a dictionary for each row ({header : value})
      reader = csv.DictReader(infile)
      
      data = {}
      for row in reader:
        
        for header, value in row.items():
          rowLengh = len(value)
          try:
            data[header].append(value)
          except KeyError:
            data[header] = [value]

    #BPOGX = data['X8']
    #BPOGY = data['Y9']

 
    #loopLen = len(BPOGX)

    FPOGX = data['FPOGX']
    FPOGY = data['FPOGX']    
    
    loopLen = len(FPOGX)

    numConvertedX = []
    numConvertedY = []
    
    for iter in range(0,loopLen):  
        numConvertedX.append(float(FPOGX[iter])*1920.0)
        
    for iter in range(0,loopLen):  
        numConvertedY.append(float(FPOGY[iter])*1080.0)
      
 
    for fruit in range(0,loopLen):  

        dt =  16

        x = numConvertedX[fruit]
        y = numConvertedY[fruit]
        
        dx = sg_filter_x.sgf(x)
        dy = sg_filter_y.sgf(y)

        state.set_mouse_velocity_coord_x(dx)
        state.set_mouse_velocity_coord_y(dy)
        
        # sampling period in s
        period = float(float(dt)/1000.0)
        dt = period * float(state.get_dfwidth())

        # window width, height
        ww = state.get_window_width()
        wh = state.get_window_height()
        r = math.sqrt(float(ww)*float(ww) + float(wh)*float(wh))
        dpi = r/float(state.get_screen())

        D = float(state.get_viewing_dist())


        fov = 2*math.degrees(math.atan2(state.get_screen(),2*D))
        fovx = 2*math.degrees(math.atan2(float(ww)/dpi,2*D))
        fovy = 2*math.degrees(math.atan2(float(wh)/dpi,2*D))


        degx = 2*math.degrees(math.atan2((dx/dpi),(2*D)))
        degy = 2*math.degrees(math.atan2((dy/dpi),(2*D)))

        # degx, degy is degrees per filter window, div by dt to get per second
        velx = degx / dt
        vely = degy / dt

        vel = math.sqrt(float(velx)*float(velx) + float(vely)*float(vely))
       
        if vel > state.get_threshold():
          state.set_fixating(0) 
          customFixeta.append(0)
          durationList.append(0)
          
        else:
          state.set_fixating(1)
          customFixeta.append(1)
          durationList.append(1)
          
    finalList = []      
    for previous, item, nxt in previous_and_next(customFixeta):
        if item == 1 and previous ==1:
            finalList.append(0)
            
        if item == 1 and previous ==0:
            finalList.append(1)           

        if item == 0:
            finalList.append(0)              
    
    for row in finalList:
       output.writerow([row])
    out.close()	  
    
    for row in durationList:
       outputD.writerow([row])
    outD.close()	    

 
def mouse_motion(x, y):
    # update mouse position
    state.set_mouse_coord_x(x)
    state.set_mouse_coord_y(y)

def refresh2d(width, height):
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glOrtho(0.0, width, 0.0, height, 0.0, 1.0)
    glMatrixMode (GL_MODELVIEW)
    glLoadIdentity()

def main():
    # init global state, rotary, and bw objects
    global state, rotary, bw_filter_x, bw_filter_y, sg_filter_x, sg_filter_y
    state = State()
    rotary = Rotary()
    state.set_window_width(1680)
    state.set_window_height(1050)
    state.set_screen(19)
    state.set_viewing_dist(22)
    #state.set_mouse_coord_x(0)
    #state.set_mouse_coord_y(0)
    state.set_dfwidth(3)
    state.set_dfdegree(2)
    state.set_dfo(1)
    state.set_threshold(20)
#   bw_filter_x = Bw(2, 60, 6.15)
#   bw_filter_y = Bw(2, 60, 6.15)
    #bw_filter_x = Bw(2, 60, 8.15)
    #bw_filter_y = Bw(2, 60, 8.15)
    # offline filter parameters seem to give too slow a response
#   bw_filter_x = Bw(2, 60, 2.35)
#   bw_filter_y = Bw(2, 60, 2.35)
    sg_filter_x = SG(state.get_dfwidth(), state.get_dfdegree(), state.get_dfo())
    sg_filter_y = SG(state.get_dfwidth(), state.get_dfdegree(), state.get_dfo())
    mouse_stream()    
    
    # init glut
#    glutInit()
#    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_ALPHA | GLUT_DEPTH)
#    window = glutCreateWindow('Rotary! Press Esc to exit')
    #glutFullScreen()
#    glutReshapeWindow(state.get_window_width(), state.get_window_height())
#    glutReshapeFunc(reshape)
#    glutDisplayFunc(display)
#    glutIdleFunc(display)
#    glutKeyboardFunc(keyboard)
#    glutTimerFunc(16,mouse_stream,16)
    # set passive motion function to eye_motion?
#    glutPassiveMotionFunc(mouse_motion)
#    glutMainLoop()

#################################
if __name__ == '__main__':
    # connect to eye tracker then call main
    main()

