#!/usr/bin/env python
# Rotary.py
# This class represents a rotary dial PIN entry screen

# system includes
import sys
from math import *
from numpy import *

from pad import Pad
from point import Point

class Rotary:
    
  def __init__(self):
    # list of smthpoints inside Pads (AOIs)
    self.smthpoints = []
    self.pads = []
    
  def addPad(self,x,y,radius,ref):
      self.pads.append(Pad(x,y,radius,ref))

  def getPad(self,x,y,tolerance):
    retVal = None
    mindis = 9999999
    for pad in self.pads:
        
      if(pad.inside(x,y,tolerance)):
          # claculate the distance to the center of the pad
          sq1 = (x-pad.getX()) ** 2
          sq2 = (y-pad.getY()) ** 2
          dis = (sq1 + sq2)
          
          if dis < mindis:
              # if the dis is shorter than a previously found pad, update it
              retVal = pad
              mindis = dis
    
    return retVal
  
  def get_pin(self,refs,width,height):
      pin = ''
      cCount = 0
      prev = ''
      # for storing votes for a number. each index refers to its number
      # ex. keys[0] is number 0, keys[5] is number 5
      keys = [(0),(0),(0),(0),(0),(0),(0),(0),(0),(0)]
      # points = points[:len(points)/2]
      #print "# of points:" + str(len(points))
      #print str(points[0].gettimestamp()), str(points[len(points)-1].gettimestamp())
      
      for ref in refs:
      # incriment cCount for each new string of c's
              if ref == 'c' and prev != 'c':
                
                 if cCount > 0 and cCount < 5:
                     
                     reff = 0
                     i = 0
                     
                     # weight the last looked at 33% higher
                     keys[int(prev)] = float(keys[int(prev)]) + float(keys[int(prev)]) * float(0.33)
                   
                     # find the key with the most votes and save the refference
                     for ky in keys:
                         if keys[i] > keys[reff]:
                             reff = i
                         i = i + 1
                 
                     pin = pin + str(reff)
                     # clear the votes when you enter back into the center
                     keys = [(0),(0),(0),(0),(0),(0),(0),(0),(0),(0)]
              
                 cCount = cCount + 1
                 
              if ref != 'c' and \
                 cCount > 0 and cCount < 5:
                 
                 # add a vote to the paticular number (reff)
                 keys[int(ref)] = keys[int(ref)] + 1
      
              prev = ref

      return (pin)
