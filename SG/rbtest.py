#!/usr/bin/env python
# was using /sw/bin/python2.7 -tt
# filter.py
# this file is the driver for the python analysis script

# system includes
import sys
import os
import getopt
import glob
#from collections import deque
import collections
from math import *
from numpy import *
import random

def main(argv):

  num_points=10

# rb = collections.deque([],num_points)
  rb = collections.deque([0.0]*num_points,num_points)

  for i in range(2*num_points+1):
    rb.appendleft(i)
    print rb
    print len(rb)

  for i in range(num_points):
    print rb[i]

if __name__ == "__main__":
 
  main(sys.argv[1:])
