#!/usr/bin/env python
# was using /sw/bin/python2.7 -tt
# filter.py
# this file is the driver for the python analysis script

# system includes
import sys
import os
import getopt
import glob
from math import *
from numpy import *
import random

from sg import SG

def main(argv):

  num_points=10
  degree=2
  diff_order=1

  x = arange(-num_points,num_points+1,dtype=int)
  monom = lambda x, deg : pow(x,deg)

  print x

  A = zeros((2*num_points+1, degree+1), float)
  for i in range(2*num_points+1):
    for j in range(degree+1):
      A[i,j] = monom(x[i], j)

  print A

  ATA = dot(A.transpose(),A)
  print ATA

  rhs = zeros((degree+1,), float)
  rhs[diff_order] = (-1)**diff_order
  print rhs

  wvec = linalg.solve(ATA, rhs)
  print wvec

  # calculate filter-coefficients
  coeff = dot(A, wvec)
  print coeff

  print "length of coeffs = %f\n" % len(coeff)

  print "sum of coeffs = %f\n" % sum(coeff)

  sgx = SG(num_points,degree,diff_order)
  sgy = SG(num_points,degree,diff_order)

  N = (num_points-1)/2

  for i in range(100):
    # feed the SG filter the same number (whatever it is) for a long time
    dxdt = sgx.sgf(1337)
#   print "len = %d" % len(dxdt)
    # and we should be seeing 0 after while (as in 0 velocity)
    print dxdt
#   print "N = %f\n" % N
#   print "len[N:-N] = %d" % len(dxdt[N:-N])
#   print dxdt[N:-N]

if __name__ == "__main__":
 
  main(sys.argv[1:])
