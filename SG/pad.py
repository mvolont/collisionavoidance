#!/usr/bin/env python
# Rotary.py
# This class represents a PIN entry screen pad (i.e., button)

# system includes
import sys
from math import *
from numpy import *

class Pad:
  def __init__(self,x,y,r,s):
    self.coord = []
    self.coord.append(x)
    self.coord.append(y)
    # radius, and radius squared
    self.radius = r
    # refference string
    self.reff = s

  def at(self,k):
    return self.coord[k]
    
  # returns 0-9, *, #, or c as a string
  def getReff(self):
    return self.reff
    
  def getX(self):
    return self.coord[0]

  def getY(self):
    return self.coord[1]

  def inside(self,x,y,tolerance=0.0):
    # compute squared distance of point (x,y) to pad center
    dist2 = (self.coord[0] - x) ** 2 + (self.coord[1] - y) ** 2

    # if squared distance is <= pad radius spuared, point (x,y) is in circle
    return dist2 <= (self.radius + tolerance) ** 2
