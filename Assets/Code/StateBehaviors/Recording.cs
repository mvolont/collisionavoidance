﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;

public class Recording : StateMachineBehaviour{
    GazeList gpList;

    float maxRayDist = 15f;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        EventManager.Instance.TriggerEvent(new EnablePlayerEvent(true));
        EventManager.Instance.TriggerEvent(new EnableMouseCursorEvent(false));

        EventManager.Instance.TriggerEvent(new SetupGazepointTrackerEvent());

        EventManager.Instance.AddListener<BroadcastGazeCommandEvent>(HandleGaze);

        gpList = new GazeList();

    }

    private void HandleGaze(BroadcastGazeCommandEvent e)
    {
        GameObject Gazed = GameObject.Find("gazeAim");
        GameObject Cam = GameObject.Find("PlaybackCamera");

        if (e.commandDict.Count > 0)
        {
            float timeInfo = Time.time;
            
            //mouse clicks
            /*int mouseInfo = 0;
            if (Input.GetMouseButtonDown(0))
            {
                mouseInfo = 1;
            }
            */
            //

            TransformInfo GazedInfo = new TransformInfo(Gazed.transform);
            TransformInfo camInfo = new TransformInfo(Cam.transform);
            PositionInfo MousePosInfo = new PositionInfo(Input.mousePosition);
            int fixInfo = VRGaze.totalFixations.Count;
            float velocityInfo = VRGaze.vel;
            string GazeIntersectionInfo = VRGaze.gazeIntersection;
            float GazeDistanceInfo =VRGaze.gazeDistance;


            gpList.GazeInfoList.Add(new GazeInfo(timeInfo, GazedInfo, camInfo, MousePosInfo, fixInfo, velocityInfo, GazeIntersectionInfo, GazeDistanceInfo));
        }

    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex){

    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        EventManager.Instance.RemoveListener<BroadcastGazeCommandEvent>(HandleGaze);
        EventManager.Instance.TriggerEvent(new SendRecordedGPListEvent(gpList));
        
    }
}

