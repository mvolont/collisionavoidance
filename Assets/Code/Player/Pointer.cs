﻿using UnityEngine;
using System.Collections;
using System;

public class Pointer : MonoBehaviour {


    void OnEnable()
    {
        EventManager.Instance.AddListener<SetPointerPosition>(HandleSetPointer);
    }

    void OnDisable()
    {
        if (EventManager.Instance)
        {
            EventManager.Instance.RemoveListener<SetPointerPosition>(HandleSetPointer);
        }
    }

    private void HandleSetPointer(SetPointerPosition e)
    {
        transform.position = e.newPos;
    }


}
