﻿using UnityEngine;
using System.Collections;

public class PlaybackCam : MonoBehaviour {

    void OnEnable()
    {
        EventManager.Instance.AddListener<SetTransformPositionEvent>(HandleSetTransform);
    }


    void OnDisable()
    {
        if (EventManager.Instance)
        {
            EventManager.Instance.RemoveListener<SetTransformPositionEvent>(HandleSetTransform);
        }
    }


    private void HandleSetTransform(SetTransformPositionEvent e)
    {
        if (e.name == transform.name)
        {
            transform.position = e.pos;
            transform.rotation = e.rot;
        }

    }
}
