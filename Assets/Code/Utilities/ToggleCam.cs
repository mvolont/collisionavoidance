﻿using UnityEngine;
using System.Collections;

public class ToggleCam : MonoBehaviour {

    public KeyCode toggleKey = KeyCode.X;

    Camera myCam;

	// Use this for initialization
	void Start () {
        myCam = GetComponent<Camera>();
	
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(toggleKey))
        {
            myCam.enabled = !myCam.enabled;
        }
	
	}
}
