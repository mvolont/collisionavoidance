﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using UnityEngine.SceneManagement;

public class TakeScreenshot : MonoBehaviour {

    public KeyCode screenshotKey = KeyCode.Insert;
    public int superSize = 1;

    string fileExtension = ".png";

    public string ScreenshotDirectory
    {
        get
        {
            return Directory.GetParent(Application.dataPath).FullName + "/Screenshots";
        }
    }

    public string FilenameBase
    {
        get
        {
            return ScreenshotDirectory + "/Screenshot_" + SceneManager.GetActiveScene().name  + fileExtension;
        }
    }

	// Update is called once per frame
	void Update () {

        //if(Event.current.type == EventType.keyUp && Event.current.keyCode == KeyCode.SysReq)
        //{

        //}

        if (Input.GetKeyDown(screenshotKey))
        {
            Debug.Log("Took Screenshot");
            Screenshot();
        }

    }


    void Screenshot()
    {
        if(!Directory.Exists(ScreenshotDirectory))
        {
            Directory.CreateDirectory(ScreenshotDirectory);
        }

        int inc = 0;
        string newFileName = FilenameBase;
        while (File.Exists(newFileName))
        {
            newFileName = FilenameBase + SceneManager.GetActiveScene().name + "_COPY_" + inc + fileExtension;
            inc++;
        }

        ScreenCapture.CaptureScreenshot(newFileName, superSize);
    }
}
