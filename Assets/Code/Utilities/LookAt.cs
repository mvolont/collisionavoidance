﻿using UnityEngine;
using System.Collections;

public class LookAt : MonoBehaviour {

    public Camera cam;
    public float objectScale = 1.0f;
    private Vector3 initialScale;
    public float maxDist = 100.0f;

	// Use this for initialization
	void Start () {

        initialScale = transform.localScale;

        if (!cam)
        {
            cam = Camera.main;
        }
        
	
	}
	
	// Update is called once per frame
	void Update () {
        transform.LookAt(cam.transform);

        Plane plane = new Plane(cam.transform.forward, cam.transform.position);
        float dist = plane.GetDistanceToPoint(transform.position);
        if(dist < maxDist)
        {
            transform.localScale = initialScale * dist * objectScale;

        } else
        {
            transform.localScale = initialScale * maxDist * objectScale;
        }

    }
}
