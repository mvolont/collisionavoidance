﻿using UnityEngine;
using System.Collections;

public class TogglePanel : MonoBehaviour {

    public KeyCode toggleKey;
    GameObject panel;

	// Use this for initialization
	void Awake () {
        panel = transform.GetChild(0).gameObject;
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(toggleKey))
        {
            panel.SetActive(!panel.activeSelf);
        }
	
	}
}
