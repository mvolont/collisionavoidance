﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HelpPanel : MonoBehaviour {

    public Text myText;

	// Use this for initialization
	void Start () {

        TogglePanel[] panels = FindObjectsOfType<TogglePanel>() as TogglePanel[];
        for(int i = 0; i < panels.Length; i++)
        {
            myText.text += panels[i].name + ": " + panels[i].toggleKey + "\n";
        }
	
	}
	

}
