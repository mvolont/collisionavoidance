﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GPText : MonoBehaviour {

    public string key;
    public List<string> attribs = new List<string>();

    Text myText;

	// Use this for initialization
	void Awake () {
        myText = GetComponent<Text>();
	}

    void OnEnable()
    {
        EventManager.Instance.AddListener<BroadcastGazeCommandEvent>(HandleGPCommand);
    }


    void OnDisable()
    {
        if (EventManager.Instance)
        {
            EventManager.Instance.RemoveListener<BroadcastGazeCommandEvent>(HandleGPCommand);
        }
    }


    private void HandleGPCommand(BroadcastGazeCommandEvent e)
    {
        string newString = key + ": ";
        foreach(string s in attribs)
        {
            newString += " " + s + ": " + e.commandDict[key].attributes[s] + " ";
        }
        myText.text = newString;
    }
}
