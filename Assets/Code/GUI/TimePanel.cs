﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimePanel : MonoBehaviour {

    public Text myText;
    float curTime = 0f;


	// Update is called once per frame
	void Update () {
        curTime += Time.deltaTime;

        float minutes = curTime / 60f;
        float seconds = curTime % 60;
        float fraction = (curTime * 100) % 100;

        myText.text = string.Format("{0:00} : {1:00} : {2:000}", minutes, seconds, fraction);

	
	}
}
