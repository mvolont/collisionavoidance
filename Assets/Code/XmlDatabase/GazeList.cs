﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization;
using System.Collections.Generic;

public class GazeList
{
    public List<GazeInfo> GazeInfoList { get; set; }

    public GazeList()
    {
        GazeInfoList = new List<GazeInfo>();
    }

    public GazeList(List<GazeInfo> SpawnInfoList)
    {
        this.GazeInfoList = SpawnInfoList;
    }

    public GazeList(SerializationInfo info, StreamingContext ctxt)
    {
        GazeInfoList = (List<GazeInfo>)info.GetValue("GazeInfoList", typeof(List<GazeInfo>));

    }

    public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
    {
        info.AddValue("GazeInfoList", GazeInfoList);

    }

}