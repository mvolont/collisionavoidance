﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization;

public class PositionInfo
{
    public float X { get; private set; }
    public float Y { get; private set; }
    public float Z { get; private set; }

    public PositionInfo() { }

    public PositionInfo(float X, float Y, float Z)
    {
        this.X = X;
        this.Y = Y;
        this.Z = Z;
    }

    public PositionInfo(Vector3 pos)
    {
        X = pos.x;
        Y = pos.y;
        Z = pos.z;
    }


    public PositionInfo(SerializationInfo info, StreamingContext ctxt)
    {
        X = (float)info.GetValue("X", typeof(float));
        Y = (float)info.GetValue("Y", typeof(float));
        Z = (float)info.GetValue("Z", typeof(float));
    }


    public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
    {
        info.AddValue("X", X);
        info.AddValue("Y", Y);
        info.AddValue("Z", Z);
    }

    public Vector3 GetPosInfo()
    {
        return new Vector3(X, Y, Z);
    }



}
