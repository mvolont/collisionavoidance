﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization;

public class TransformInfo
{
    public string Name { get; private set; }
    public PositionInfo Pos { get; private set; }
    public RotationInfo Rot { get; private set; }



    public TransformInfo() { }

    public TransformInfo(string Name, PositionInfo Pos, RotationInfo Rot)
    {
        this.Name = Name;
        this.Pos = Pos;
        this.Rot = Rot;
    }

    public TransformInfo(Transform trans)
    {
        Name = trans.name;
        Pos = new PositionInfo(trans.position);
        Rot = new RotationInfo(trans.rotation);
    }


    public TransformInfo(SerializationInfo info, StreamingContext ctxt)
    {
        Name = (string)info.GetValue("Name", typeof(string));
        Pos = (PositionInfo)info.GetValue("Pos", typeof(PositionInfo));
        Rot = (RotationInfo)info.GetValue("Rot", typeof(RotationInfo));
    }


    public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
    {
        info.AddValue("Name", Name);
        info.AddValue("Pos", Pos);
        info.AddValue("Rot", Rot);

    }
}
