﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization;

public class TimeInfo { 

    public float Time { get; set; }


    public TimeInfo()
    {
        Time = 0f;
    }

    public TimeInfo(float Time)
    {
        this.Time = Time;
    }

    public TimeInfo(SerializationInfo info, StreamingContext ctxt){
        Time = (float)info.GetValue("Time", typeof(float));
    }

    public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
    {
        info.AddValue("TimeInfo", Time);
    }
}


