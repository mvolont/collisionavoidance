﻿using System.Xml.Serialization;
using System.Xml;
using System.IO;
using UnityEngine;

// adapted from: http://www.dreamincode.net/forums/topic/191471-reading-and-writing-xml-using-serialization/

public class MyXML  {

    public static bool SaveObject<T>(T obj, string outputDataDirectory, string subDirectory, string identifier, string fileName)
    {
        if (!Directory.Exists(Path.Combine(outputDataDirectory, subDirectory)))
        {
            Directory.CreateDirectory(Path.Combine(outputDataDirectory, subDirectory));
        }

        string fileExtension = ".xml";
        string filenameBase = Path.Combine(outputDataDirectory, subDirectory) + "/" + fileName;
        string newFilename = filenameBase + identifier + fileExtension;
        int inc = 0;

        while (File.Exists(newFilename))
        {
            newFilename = filenameBase + identifier + "_COPY_" + inc + fileExtension;
            inc++;
        }

        try
        {
            var x = new XmlSerializer(obj.GetType());
            using (var Writer = new StreamWriter(newFilename, false))
            {
                x.Serialize(Writer, obj);
            }
            return true;
        }
        catch
        {
            return false;
        }
    }

    public static bool GetObject<T>(ref T obj, string FileName)
    {
        try
        {
            using (FileStream stream = new FileStream(FileName, FileMode.Open))
            {
                XmlTextReader reader = new XmlTextReader(stream);
                var x = new XmlSerializer(obj.GetType());
                obj = (T)x.Deserialize(reader);
                return true;
            }
        }
        catch
        {
        }
        return false;
    }

    public static bool GetObject<T>(ref T obj, TextAsset textAsset)
    {
        try
        {
            using (var reader = new System.IO.StringReader(textAsset.text))
            {
                var x = new XmlSerializer(obj.GetType());
                obj = (T)x.Deserialize(reader);
                return true;
            }
        }
        catch { }
        return false;
    }





}
