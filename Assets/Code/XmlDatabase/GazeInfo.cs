﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization;

public class GazeInfo
    {
    public float TimeInfo { get; set; }
    public TransformInfo GazedInfo { get; set; }
    public TransformInfo CamInfo { get; set; }
    public PositionInfo MousePosInfo { get; set; }
    public int MouseInfo { get; set; }
    public int FixationInfo { get; set; }
    public float VelocityInfo { get; set; }
    public string GazeIntersectInfo { get; set; }
    public float GazeDistanceInfo { get; set; }

    public GazeInfo()
    {

        TimeInfo = 0f;
    }

    public GazeInfo(float TimeInfo, TransformInfo GazedInfo, TransformInfo CamInfo, PositionInfo MousePosInfo, int FixationInfo, float VelocityInfo, string GazeIntersectInfo, float GazeDistanceInfo)
    {
        this.TimeInfo = TimeInfo;
        this.GazedInfo = GazedInfo;
        this.CamInfo = CamInfo;
        this.MousePosInfo = MousePosInfo;
        this.FixationInfo = FixationInfo;
        this.VelocityInfo = VelocityInfo;
        this.GazeIntersectInfo = GazeIntersectInfo;
        this.GazeDistanceInfo = GazeDistanceInfo;
    }

    public GazeInfo(SerializationInfo info, StreamingContext ctxt)
    {
        TimeInfo = (float)info.GetValue("TimeInfo", typeof(float));
        GazedInfo = (TransformInfo)info.GetValue("GazedInfo", typeof(TransformInfo));
        CamInfo = (TransformInfo)info.GetValue("CamInfo", typeof(TransformInfo));
        MousePosInfo = (PositionInfo)info.GetValue("MousePosInfo", typeof(PositionInfo));
        MouseInfo = (int)info.GetValue("MouseInfo", typeof(int));
        FixationInfo = (int)info.GetValue("FixationInfo", typeof(int));
        VelocityInfo = (float)info.GetValue("VelocityInfo", typeof(float));
        GazeIntersectInfo = (string)info.GetValue("GazeIntersectInfo", typeof(string));
        GazeDistanceInfo = (float)info.GetValue("GazeDistanceInfo", typeof(float));
    }

    public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
    {
        info.AddValue("TimeInfo", TimeInfo);
        info.AddValue("GazedInfo", GazedInfo);
        info.AddValue("CamInfo", CamInfo);
        info.AddValue("MouseInfo", MouseInfo);
        info.AddValue("FixationInfo", FixationInfo);      
        info.AddValue("VelocityInfo", VelocityInfo);      
        info.AddValue("GazeIntersectInfo", GazeIntersectInfo);      
        info.AddValue("GazeDistanceInfo", GazeDistanceInfo);      
    }
}


