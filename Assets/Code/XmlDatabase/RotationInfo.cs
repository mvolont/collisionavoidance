﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization;

public class RotationInfo
{

    public float X { get; private set; }
    public float Y { get; private set; }
    public float Z { get; private set; }
    public float W { get; private set; }


    public RotationInfo() { }

    public RotationInfo(float X, float Y, float Z, float W)
    {
        this.X = X;
        this.Y = Y;
        this.Z = Z;
        this.W = W;
    }

    public RotationInfo(Quaternion rot)
    {
        X = rot.x;
        Y = rot.y;
        Z = rot.z;
        W = rot.w;
    }


    public RotationInfo(SerializationInfo info, StreamingContext ctxt)
    {
        X = (float)info.GetValue("X", typeof(float));
        Y = (float)info.GetValue("Y", typeof(float));
        Z = (float)info.GetValue("Z", typeof(float));
        W = (float)info.GetValue("W", typeof(float));

    }


    public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
    {
        info.AddValue("X", X);
        info.AddValue("Y", Y);
        info.AddValue("Z", Z);
        info.AddValue("W", W);

    }



}

