﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine;
using Tobii.XR.GazeModifier;



public class DisplayGazeData : MonoBehaviour
{
    public Text labelFixations;
    public Text Velocity;
    public Text labelTransformName;
    public Text labelDistance;


    // Update is called once per frame
    void Update()
    {

        if (ScenarioManager.recordingStarted)
        {
            try
            {
                labelFixations.text = string.Format("{0:00}", VRGaze.totalFixations.Count.ToString());
                Velocity.text = string.Format("{0:00}", VRGaze.vel.ToString());
                labelTransformName.text = string.Format("{0:00}", VRGaze.gazeIntersection);
                labelDistance.text = string.Format("{0:00}", VRGaze.gazeDistance.ToString());
            }
            catch
            {
                Debug.Log("Gaze is not interesecting geometry");
            }
        }

        if (ScenarioManager.replayingStarted) {
            try
            {
                labelFixations.text = string.Format("{0:00}", ReplayManager.fixationValue);
                Velocity.text = string.Format("{0:00}", ReplayManager.velocityValue.ToString());
                labelTransformName.text = string.Format("{0:00}", ReplayManager.gazingAt);
                labelDistance.text = string.Format("{0:00}", ReplayManager.gazingDistance.ToString());
            }
            catch {
                Debug.Log("Gaze is not interesecting geometry");
            }
        }
    }
}
