﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class Selectable : MonoBehaviour {

    public float totalTimeLookingAt = 0f;
    bool isLookingAt;

    public Text label;

    public MeshRenderer outlineShader;
    public Material highlightMat;
    public Material baseMat;


    void OnEnable()
    {
        EventManager.Instance.AddListener<SetLookingAtEvent>(HandleLookingAt);
    }

    void OnDisable()
    {
        if (EventManager.Instance)
        {
            EventManager.Instance.RemoveListener<SetLookingAtEvent>(HandleLookingAt);
        }
    }

    private void HandleLookingAt(SetLookingAtEvent e)
    {
        if (e.lookingAt == transform)
        {
            isLookingAt = true;
        }
        else
        {
            isLookingAt = false;
        }

    }

    // Update is called once per frame
    void Update () {
        if (isLookingAt)
        {
            totalTimeLookingAt += Time.deltaTime;
            outlineShader.material = highlightMat;
        }
        else
        {
            outlineShader.material = baseMat;
        }

        float minutes = totalTimeLookingAt / 60f;
        float seconds = totalTimeLookingAt % 60;
        float fraction = (totalTimeLookingAt * 100) % 100;

        label.text = string.Format("{0:00} : {1:00} : {2:00}", minutes, seconds, fraction);

    }


}
