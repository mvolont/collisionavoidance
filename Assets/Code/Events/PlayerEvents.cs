﻿using UnityEngine;
using System.Collections;

public class EnablePlayerEvent : GameEvent
{
    public bool isOn { get; private set; }

    public EnablePlayerEvent(bool isOn)
    {
        this.isOn = isOn;
    }
}

public class EnableMouseCursorEvent : GameEvent
{
    public bool isOn { get; private set; }

    public EnableMouseCursorEvent(bool isOn)
    {
        this.isOn = isOn;
    }
}


public class CameraZoomChangedEvent : GameEvent
{

    public float minFOV { get; set; }
    public float maxFOV { get; set; }

    public float curFOV { get; set; }
    public float sign { get; set; }


    public CameraZoomChangedEvent(float minFOV, float maxFOV, float curFOV)
    {
        this.minFOV = minFOV;
        this.maxFOV = maxFOV;
        this.curFOV = curFOV;
    }

    public CameraZoomChangedEvent(float minFOV, float maxFOV, float curFOV, float sign)
    {
        this.minFOV = minFOV;
        this.maxFOV = maxFOV;
        this.curFOV = curFOV;
        this.sign = sign;
    }

}

public class SetPointerPosition : GameEvent
{
    public Vector3 newPos { get; set; }

    public SetPointerPosition(Vector3 newPos)
    {
        this.newPos = newPos;
    }
}

public class SetLookingAtEvent : GameEvent
{
    public Transform lookingAt { get; set; }
    public float deltaTime { get; set; }

    public SetLookingAtEvent(Transform lookingAt)
    {
        this.lookingAt = lookingAt;
    }

    public SetLookingAtEvent(Transform lookingAt, float deltaTime)
    {
        this.lookingAt = lookingAt;
        this.deltaTime = deltaTime;
    }
}

public class SetTransformPositionEvent : GameEvent
{
    public string name { get; private set; }
    public Vector3 pos { get; private set; }
    public Quaternion rot { get; private set; }

    public SetTransformPositionEvent(string name, Vector3 pos, Quaternion rot)
    {
        this.name = name;
        this.pos = pos;
        this.rot = rot;
    }


}
