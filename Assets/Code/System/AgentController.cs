﻿#pragma warning disable 0649 // Field is never assigned to, and will always have its default value

using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;
using System.IO;

namespace Agent
{
    public enum AGENT_STATE
    {
        idle,
        walk,
        run     //add but no use
    }

    public enum IDLE_STYLE
    {
        standard,
        happy,
        sad,
        lookingAround,
        angry
    }

    public enum WALKING_STYLE
    {
        standard,
        sad,
        female,
        strut
    }

    public class AgentController : MonoBehaviour
    {
        public static AgentController instance;

        public float agentWalkSpeed = 1.4f;
        public int idxOfCharacter = 0;
        public float peripersonalSpace = 125.0f;
        public float angleOfGazeUser = 80.0f;
        public float headRotateSpeed = 3.0f;

        public bool needLookAtUser = true;

        public GameObject[] CharacterPrefabe { get; private set; }

        [SerializeField] private RuntimeAnimatorController animatorController;
        [SerializeField] private Transform walkDestination1;
        [SerializeField] private Transform walkDestination2;

        public GameObject Agent { get; private set; }
        private HeadController headRotation;
        private Animator animator;
        private AGENT_STATE agentState = AGENT_STATE.idle;
        private WALKING_STYLE walkStyle = WALKING_STYLE.standard;
        private IDLE_STYLE idleStyle = IDLE_STYLE.standard;

        private Transform destination = null;

        private AnimatorOverrideController overrideController;
        private Dictionary<string, AnimationClip> walkingAnimClips = new Dictionary<string, AnimationClip>();
        private Dictionary<string, AnimationClip> idleAnimClips = new Dictionary<string, AnimationClip>();

        void Awake()
        {
            instance = this;
        }

        public void LoadModelAndAnimationClip()
        {
            CharacterPrefabe = Resources.LoadAll<GameObject>("Characters");

            AnimationClip[] walkAnims = Resources.LoadAll<AnimationClip>("Animation/WalkingStyle");
            foreach (AnimationClip clip in walkAnims)
            {
                walkingAnimClips[clip.name] = clip;
            }

            AnimationClip[] idleAnims = Resources.LoadAll<AnimationClip>("Animation/IdleStyle");
            foreach (AnimationClip clip in idleAnims)
            {
                idleAnimClips[clip.name] = clip;
            }

            overrideController = new AnimatorOverrideController(animatorController);

        }

        public void StartAgentControl()
        {
            destination = null;
            //User.instance.userBody.SetActive(true);
            StartCoroutine(WalkingSimulation());
        }

        public void StopAgentControl()
        {
            StopAllCoroutines();
            if (Agent)
            {
                Destroy(Agent);
            }
        }

        private IEnumerator WalkingSimulation()
        {
            while (true)
            {
                //Set agent destination
                destination = (destination != walkDestination1) ? walkDestination1 : walkDestination2;

                //Set User initial position, forward and destination.
                Vector3 userInitPos = destination.position;
                userInitPos.y = User.instance.transform.position.y;
                User.instance.transform.position = userInitPos;
                User.instance.destination = (destination != walkDestination1) ? walkDestination1 : walkDestination2;
                User.instance.transform.forward = User.instance.destination.position - destination.position;

                //Creat a new character
                int agentIndex = (idxOfCharacter >= CharacterPrefabe.Length) ? Random.Range(0, CharacterPrefabe.Length) : idxOfCharacter;
                ParameterSetting.instance.SetCurrentModelNameOnUI(agentIndex);
                Agent = Instantiate(CharacterPrefabe[agentIndex], transform);

                //Set initial position and direction of agent
                Vector3 originPosition = (destination == walkDestination1) ? walkDestination2.position : walkDestination1.position;
                Agent.transform.position = originPosition;
                Agent.transform.forward = destination.position - originPosition;

                headRotation = Agent.AddComponent<HeadController>();
                headRotation.Init();

                //Height of agent
                SkinnedMeshRenderer skinnedMeshRenderer = Agent.GetComponentInChildren<SkinnedMeshRenderer>();
                float height = skinnedMeshRenderer.bounds.extents.y * 2;
                Agent.transform.localScale = Vector3.one * (1.8f / height);

                animator = Agent.GetComponent<Animator>();
                animator.runtimeAnimatorController = overrideController;

                SetAgentState(AGENT_STATE.idle);

                yield return StartCoroutine(TrafficLightController.instance.SetTrafficLight());
                RecordManager.instance.StartRecord();

                SetAgentState(AGENT_STATE.walk);
                yield return StartCoroutine(Walking());
                SetAgentState(AGENT_STATE.idle);


                yield return new WaitUntil(() => User.instance.ReachDestination());
                RecordManager.instance.StopRecord();

                //Delete old agent
                Destroy(Agent);
            }

        }

        private IEnumerator Walking()
        {
            NavMeshPath path = new NavMeshPath();

            // while (true)
            // {
            //     //Moving
            //     NavMesh.CalculatePath(agent.transform.position, destination.position, NavMesh.AllAreas, path);
            //     if (path.corners.Length <= 1)
            //     {
            //         break;
            //     }
            //     animator.speed = AnimationSpeedBaseOnWalkSpeed();         //Walking speed can change dynamically
            //     float maxDistance = agentWalkSpeed * Time.deltaTime;
            //     Vector3 nextPosition = Vector3.MoveTowards(agent.transform.position, path.corners[1], maxDistance);
            //     agent.transform.forward = Vector3.Slerp(agent.transform.forward, nextPosition - agent.transform.position, Time.deltaTime * 10);
            //     agent.transform.position = nextPosition;



            //     //Gaze
            //     bool needLookUser = false;
            //     if (Vector3.Distance(User.instance.userBody.transform.position, agent.transform.position) < peripersonalSpace)
            //     {
            //         if (Vector3.Angle(agent.transform.forward, (User.instance.userBody.transform.position - agent.transform.position)) < angleOfGazeUser)
            //         {
            //             needLookUser = true;
            //         }
            //     }
            //     headRotation.LookAtUser = needLookUser;


            //     //Debug (Draw trajectory and peripersonal Space )
            //     for (int i = 0; i < path.corners.Length - 1; i++)
            //     {
            //         Debug.DrawLine(path.corners[i], path.corners[i + 1], Color.red);
            //     }

            //     temp.Clear();
            //     temp.Add(agent.transform.position + agent.transform.right * peripersonalSpace);

            //     int samplePoint = 30;
            //     for (int i = 0; i < samplePoint / 2; i++)
            //     {
            //         temp.Add(agent.transform.position + Vector3.Slerp(agent.transform.right, agent.transform.forward, i / (samplePoint / 2.0f)) * peripersonalSpace);
            //     }
            //     for (int i = 0; i < samplePoint / 2; i++)
            //     {
            //         temp.Add(agent.transform.position + Vector3.Slerp(agent.transform.forward, agent.transform.right * -1, i / (samplePoint / 2.0f)) * peripersonalSpace);
            //     }
            //     temp.Add(agent.transform.position + agent.transform.right * peripersonalSpace * -1);
            //     temp.Add(agent.transform.position + agent.transform.right * peripersonalSpace);

            //     for (int i = 0; i < temp.Count - 1; i++)
            //     {
            //         Debug.DrawLine(temp[i], temp[i + 1], Color.blue);
            //     }

            //     yield return null;
            // }
            // headRotation.LookAtUser = false;

            NavMesh.CalculatePath(Agent.transform.position, destination.position, NavMesh.AllAreas, path);
            foreach (Vector3 corner in path.corners)
            {
                while (Agent.transform.position != corner)
                {
                    //Moving
                    animator.speed = AnimationSpeedBaseOnWalkSpeed();         //Walking speed can change dynamically
                    float maxDistance = agentWalkSpeed * Time.deltaTime;
                    Vector3 nextPosition = Vector3.MoveTowards(Agent.transform.position, corner, maxDistance);
                    Agent.transform.position = nextPosition;

                    //Debug (Draw trajectory)
                    for (int i = 0; i < path.corners.Length - 1; i++)
                    {
                        Debug.DrawLine(path.corners[i], path.corners[i + 1], Color.red);
                    }

                    yield return null;
                }
            }

        }

        private void SetAgentState(AGENT_STATE _STATE)
        {
            if (Agent)
            {
                agentState = _STATE;
                switch (_STATE)
                {
                    case AGENT_STATE.idle:
                        animator.SetTrigger("idle");
                        animator.speed = 1.0f;
                        break;

                    case AGENT_STATE.walk:
                        animator.SetTrigger("walk");
                        //SetAgentWalkStyle(walkStyle);
                        break;

                    case AGENT_STATE.run:
                        animator.SetTrigger("run");
                        break;
                }
            }
        }

        private float AnimationSpeedBaseOnWalkSpeed()
        {
            float speed = 0.0f;
            switch (walkStyle)
            {
                case WALKING_STYLE.standard:
                    speed = agentWalkSpeed / 1.5f;
                    break;

                case WALKING_STYLE.sad:
                    speed = agentWalkSpeed / 0.8f;
                    break;

                case WALKING_STYLE.female:
                    speed = agentWalkSpeed / 1.1f;
                    break;

                case WALKING_STYLE.strut:
                    speed = agentWalkSpeed / 0.9f;
                    break;
            }
            return speed;
        }

        public void SetAgentWalkStyle(WALKING_STYLE _STYLE)
        {
            walkStyle = _STYLE;
            switch (_STYLE)
            {
                case WALKING_STYLE.standard:
                    overrideController["StandardWalk"] = walkingAnimClips["StandardWalk"];
                    break;

                case WALKING_STYLE.sad:
                    overrideController["StandardWalk"] = walkingAnimClips["SadWalk"];
                    break;

                case WALKING_STYLE.female:
                    overrideController["StandardWalk"] = walkingAnimClips["FemaleWalk"];
                    break;

                case WALKING_STYLE.strut:
                    overrideController["StandardWalk"] = walkingAnimClips["StrutWalking"];
                    break;
            }
        }

        public void SetAgentIdleStyle(IDLE_STYLE _STYLE)
        {
            idleStyle = _STYLE;
            switch (_STYLE)
            {
                case IDLE_STYLE.standard:
                    overrideController["StandingIdle"] = idleAnimClips["StandingIdle"];
                    break;

                case IDLE_STYLE.happy:
                    overrideController["StandingIdle"] = idleAnimClips["HappyIdle"];
                    break;

                case IDLE_STYLE.lookingAround:
                    overrideController["StandingIdle"] = idleAnimClips["LookingAround"];
                    break;

                case IDLE_STYLE.sad:
                    overrideController["StandingIdle"] = idleAnimClips["SadIdle"];
                    break;

                case IDLE_STYLE.angry:
                    overrideController["StandingIdle"] = idleAnimClips["Angry"];
                    break;
            }
        }

    }

}

