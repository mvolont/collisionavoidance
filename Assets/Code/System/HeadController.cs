﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Agent;

public class HeadController : MonoBehaviour
{
    private Transform m_Head;
    private Transform m_DummyHeadWordSpace;
    private Transform m_DummyHeadLocalSpace;
    private Quaternion currentRotation;

    private Transform m_LeftEye;
    private Transform m_DummyLeftEyeWordSpace;
    private Transform m_DummyLeftEyeLocalSpace;
    private Quaternion leftEyeInitRot;

    private Transform m_RightEye;
    private Transform m_DummyRightEyeWordSpace;
    private Transform m_DummyRightEyeLocalSpace;
    private Quaternion rightEyeInitRot;


    private Transform m_RightArm;
    private Transform m_LeftArm;



    public void Init()
    {
        //Head Rotation
        GameObject dummyHeadWorldObj = new GameObject("DummyHeadWordSpace");
        m_DummyHeadWordSpace = dummyHeadWorldObj.transform;
        GameObject dummyHeadLocalObj = new GameObject("DummyHeadLocalSpace");
        m_DummyHeadLocalSpace = dummyHeadLocalObj.transform;

        m_DummyHeadLocalSpace.parent = m_DummyHeadWordSpace;
        m_DummyHeadWordSpace.parent = this.transform;
        m_DummyHeadLocalSpace.localPosition = Vector3.zero;

        m_Head = FindChildInHierarchy("Bip01 Head");

        m_DummyHeadWordSpace.position = m_Head.position;
        m_DummyHeadWordSpace.forward = transform.forward;

        m_DummyHeadLocalSpace.rotation = m_Head.rotation;

        currentRotation = m_Head.rotation;

        //Left Eye Rotation
        GameObject dummyLeftEyeWorldObj = new GameObject("DummyLeftEyeWordSpace");
        m_DummyLeftEyeWordSpace = dummyLeftEyeWorldObj.transform;
        GameObject dummyLeftEyeLocalObj = new GameObject("DummyLeftEyeLocalSpace");
        m_DummyLeftEyeLocalSpace = dummyLeftEyeLocalObj.transform;

        m_DummyLeftEyeLocalSpace.parent = m_DummyLeftEyeWordSpace;
        m_DummyLeftEyeWordSpace.parent = this.transform;
        m_DummyLeftEyeLocalSpace.localPosition = Vector3.zero;

        m_LeftEye = FindChildInHierarchy("Bip01 LEye");
        leftEyeInitRot = m_LeftEye.localRotation;

        m_DummyLeftEyeWordSpace.position = m_LeftEye.position;
        m_DummyLeftEyeWordSpace.forward = transform.forward;

        m_DummyLeftEyeLocalSpace.rotation = m_LeftEye.rotation;


        //Right Eye Rotation
        GameObject dummyRightEyeWorldObj = new GameObject("DummyRightEyeWordSpace");
        m_DummyRightEyeWordSpace = dummyRightEyeWorldObj.transform;
        GameObject dummyRightEyeLocalObj = new GameObject("DummyRightEyeLocalSpace");
        m_DummyRightEyeLocalSpace = dummyRightEyeLocalObj.transform;

        m_DummyRightEyeLocalSpace.parent = m_DummyRightEyeWordSpace;
        m_DummyRightEyeWordSpace.parent = this.transform;
        m_DummyRightEyeLocalSpace.localPosition = Vector3.zero;

        m_RightEye = FindChildInHierarchy("Bip01 REye");
        rightEyeInitRot = m_LeftEye.localRotation;

        m_DummyRightEyeWordSpace.position = m_RightEye.position;
        m_DummyRightEyeWordSpace.forward = transform.forward;

        m_DummyRightEyeLocalSpace.rotation = m_RightEye.rotation;

        m_RightArm = FindChildInHierarchy("Bip01 R Clavicle");
        m_LeftArm = FindChildInHierarchy("Bip01 L Clavicle");

    }

    private void LateUpdate()
    {
        if (!AgentController.instance.needLookAtUser) return;


        m_DummyHeadWordSpace.position = m_Head.position;

        Vector3 userPos = User.instance.transform.position;
        userPos.y = 0;
        Vector3 agentPos = transform.position;
        agentPos.y = 0;
        float distance = Vector3.Distance(userPos, agentPos);

        float angle = Vector3.Angle(transform.forward, (userPos - agentPos));

        if (distance < AgentController.instance.peripersonalSpace && angle <= AgentController.instance.angleOfGazeUser / 2)
        {
            if (angle > 80.0f)
            {
                if (Vector3.Dot((userPos - agentPos), transform.right) > 0)
                {
                    m_DummyHeadWordSpace.forward = Rotate(transform.forward, -80.0f);
                }
                else
                {
                    m_DummyHeadWordSpace.forward = Rotate(transform.forward, 80.0f);
                }

            }
            else
            {
                m_DummyHeadWordSpace.LookAt(User.instance.transform.position, Vector3.up);
            }


            currentRotation = Quaternion.Slerp(currentRotation, m_DummyHeadLocalSpace.rotation, Time.deltaTime * AgentController.instance.headRotateSpeed);
        }
        else
        {
            currentRotation = Quaternion.Slerp(currentRotation, m_Head.rotation, Time.deltaTime * AgentController.instance.headRotateSpeed);    //original rotation of anumation.
        }
        m_Head.rotation = currentRotation;


        /* Must wait for m_Head.rotation then rotate the eye */
        m_DummyLeftEyeWordSpace.position = m_LeftEye.position;
        m_DummyRightEyeWordSpace.position = m_RightEye.position;

        if (distance < AgentController.instance.peripersonalSpace && angle <= AgentController.instance.angleOfGazeUser / 2)
        {
            m_DummyLeftEyeWordSpace.LookAt(User.instance.transform.position, Vector3.up);
            m_DummyRightEyeWordSpace.LookAt(User.instance.transform.position, Vector3.up);
            m_LeftEye.rotation = Quaternion.Slerp(m_LeftEye.rotation, m_DummyLeftEyeLocalSpace.rotation, Time.deltaTime * AgentController.instance.headRotateSpeed);
            m_RightEye.rotation = Quaternion.Slerp(m_RightEye.rotation, m_DummyRightEyeLocalSpace.rotation, Time.deltaTime * AgentController.instance.headRotateSpeed);
        }
        else
        {
            m_LeftEye.localRotation = Quaternion.Slerp(m_LeftEye.localRotation, leftEyeInitRot, Time.deltaTime * AgentController.instance.headRotateSpeed);
            m_RightEye.localRotation = Quaternion.Slerp(m_RightEye.localRotation, rightEyeInitRot, Time.deltaTime * AgentController.instance.headRotateSpeed);
        }


        // Debug (Draw peripersonal space)
        int samplePoint = 30;
        Vector3 leftArmPosition = m_LeftArm.position;
        leftArmPosition.y = transform.position.y;
        Vector3 rightArmPosition = m_RightArm.position;
        rightArmPosition.y = transform.position.y;

        Vector3 leftDirection = Rotate(transform.forward, AgentController.instance.angleOfGazeUser / 2);
        Vector3 lastPoint = leftArmPosition + leftDirection * AgentController.instance.peripersonalSpace;
        Debug.DrawLine(leftArmPosition, lastPoint, Color.blue);
        for (int i = 0; i < samplePoint / 2; i++)
        {
            //temp.Add(agent.transform.position + Vector3.Slerp(agent.transform.right, agent.transform.forward, i / (samplePoint / 2.0f)) * peripersonalSpace);
            Vector3 currentPoint = leftArmPosition + Vector3.Slerp(leftDirection, transform.forward, i / (samplePoint / 2.0f)) * AgentController.instance.peripersonalSpace;
            Debug.DrawLine(lastPoint, currentPoint, Color.blue);
            lastPoint = currentPoint;
        }
        Vector3 rightDirection = Rotate(transform.forward, -AgentController.instance.angleOfGazeUser / 2);
        for (int i = 0; i < samplePoint / 2; i++)
        {
            Vector3 currentPoint = rightArmPosition + Vector3.Slerp(transform.forward, rightDirection, i / (samplePoint / 2.0f)) * AgentController.instance.peripersonalSpace;
            Debug.DrawLine(lastPoint, currentPoint, Color.blue);
            lastPoint = currentPoint;
        }
        Debug.DrawLine(lastPoint, rightArmPosition + rightDirection * AgentController.instance.peripersonalSpace, Color.blue);
        Debug.DrawLine(rightArmPosition + rightDirection * AgentController.instance.peripersonalSpace, rightArmPosition, Color.blue);


        //Camera.main.transform.position = m_DummyHeadWordSpace.position + transform.forward * 1.0f;
        //Camera.main.transform.forward = transform.forward * -1;


    }

    private Transform FindChildInHierarchy(string name)
    {
        Transform[] children = transform.GetComponentsInChildren<Transform>();
        foreach (var child in children)
        {
            if (child.name == name)
            {
                return child;
            }
        }
        return null;
    }

    private Vector3 Rotate(Vector3 v, float degree)
    {
        float sin = Mathf.Sin(degree * Mathf.Deg2Rad);
        float cos = Mathf.Cos(degree * Mathf.Deg2Rad);
        return new Vector3(v.x * cos - v.z * sin, 0, v.x * sin + v.z * cos);
    }

}
