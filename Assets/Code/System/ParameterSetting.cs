﻿#pragma warning disable 0649 // Field is never assigned to, and will always have its default value
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Agent;
using TMPro;

public class ParameterSetting : MonoBehaviour
{
    public static ParameterSetting instance;

    [SerializeField] private GameObject m_Panel;
    [SerializeField] private TextMeshProUGUI m_ButtonText;


    [SerializeField] private Slider m_WalkingSpeedSlider;
    [SerializeField] private TextMeshProUGUI m_WalkingSpeedText;

    [SerializeField] private TMP_Dropdown m_CharacterModelDropdown;
    [SerializeField] private TextMeshProUGUI m_CharacterModelText;

    [SerializeField] private TMP_Dropdown m_WalkingStyleDropdown;
    [SerializeField] private TextMeshProUGUI m_WalkingStyleText;

    [SerializeField] private TMP_Dropdown m_IdleStyleDropdown;
    [SerializeField] private TextMeshProUGUI m_IdleStyleText;


    [SerializeField] private TMP_Dropdown m_WaitingTimeDropdown;
    [SerializeField] private TMP_Dropdown m_WalkingTimeDropdown;


    [SerializeField] private Toggle m_LookAtUserToggle;

    [SerializeField] private Slider m_PeripersonalSpaceSlider;
    [SerializeField] private TextMeshProUGUI m_PeripersonalSpaceText;

    [SerializeField] private Slider m_GazeAngleSlider;
    [SerializeField] private TextMeshProUGUI m_GazeAngleText;

    [SerializeField] private Slider m_HeadRotateSpeedSlider;
    [SerializeField] private TextMeshProUGUI m_HeadRotateSpeedText;

    private void Awake()
    {
        instance = this;

        m_Panel.SetActive(false);
        m_ButtonText.text = "Setting";
    }

    public void Init()
    {
        // m_WalkingSpeedSlider
        m_WalkingSpeedSlider.minValue = 1.0f;
        m_WalkingSpeedSlider.maxValue = 2.5f;
        m_WalkingSpeedSlider.onValueChanged.AddListener((float value) =>
        {
            AgentController.instance.agentWalkSpeed = value;
            m_WalkingSpeedText.text = value.ToString("0.00") + " m/s";
        });
        m_WalkingSpeedSlider.value = 1.5f;


        // m_CharacterModelDropdown
        m_CharacterModelDropdown.ClearOptions();
        List<string> m_DropOptions = new List<string>();
        foreach (GameObject prefabe in AgentController.instance.CharacterPrefabe)
        {
            m_DropOptions.Add(prefabe.name);
        }
        m_DropOptions.Add("Random");
        m_CharacterModelDropdown.AddOptions(m_DropOptions);

        m_CharacterModelDropdown.onValueChanged.AddListener((int idx) =>
        {
            AgentController.instance.idxOfCharacter = idx;
        });

        m_CharacterModelDropdown.value = m_CharacterModelDropdown.options.Count - 1;    //Set "Random" for beginning


        //m_WalkingStyleDropdown
        m_WalkingStyleDropdown.ClearOptions();
        m_DropOptions.Clear();
        m_DropOptions.Add(WALKING_STYLE.standard.ToString());
        m_DropOptions.Add(WALKING_STYLE.sad.ToString());
        m_DropOptions.Add(WALKING_STYLE.female.ToString());
        m_DropOptions.Add(WALKING_STYLE.strut.ToString());
        m_WalkingStyleDropdown.AddOptions(m_DropOptions);
        m_WalkingStyleDropdown.onValueChanged.AddListener((int idx) =>
        {
            AgentController.instance.SetAgentWalkStyle((WALKING_STYLE)idx);
            m_WalkingStyleText.text = m_WalkingStyleDropdown.options[idx].text;
        });
        AgentController.instance.SetAgentWalkStyle(WALKING_STYLE.standard);


        //m_IdleStyleDropdown
        m_IdleStyleDropdown.ClearOptions();
        m_DropOptions.Clear();
        m_DropOptions.Add(IDLE_STYLE.standard.ToString());
        m_DropOptions.Add(IDLE_STYLE.happy.ToString());
        m_DropOptions.Add(IDLE_STYLE.sad.ToString());
        m_DropOptions.Add(IDLE_STYLE.lookingAround.ToString());
        m_DropOptions.Add(IDLE_STYLE.angry.ToString());
        m_IdleStyleDropdown.AddOptions(m_DropOptions);
        m_IdleStyleDropdown.onValueChanged.AddListener((int idx) =>
        {
            AgentController.instance.SetAgentIdleStyle((IDLE_STYLE)idx);
            m_IdleStyleText.text = m_IdleStyleDropdown.options[idx].text;
        });
        AgentController.instance.SetAgentIdleStyle(IDLE_STYLE.standard);



        // m_WaitingTimeDropdown
        // m_WalkingTimeDropdown
        m_WaitingTimeDropdown.ClearOptions();
        //m_WalkingTimeDropdown.ClearOptions();
        m_DropOptions.Clear();
        for (int i = 1; i <= 15; i++)
        {
            m_DropOptions.Add(i.ToString() + "s");
        }

        m_WaitingTimeDropdown.AddOptions(m_DropOptions);
        //m_WalkingTimeDropdown.AddOptions(m_DropOptions);

        m_WaitingTimeDropdown.onValueChanged.AddListener((int idx) =>
        {
            TrafficLightController.instance.m_WaitTime = idx + 1;
        });
        //m_WalkingTimeDropdown.onValueChanged.AddListener((int idx) =>
        //{
        //    TrafficLightController.instance.m_WalkTime = idx + 1;
        //});

        m_WaitingTimeDropdown.value = 4;    //5s
        //m_WalkingTimeDropdown.value = 9;    //10s


        //m_LookAtUserToggle
        m_LookAtUserToggle.onValueChanged.AddListener((bool isOn) =>
        {
            m_PeripersonalSpaceSlider.interactable = isOn;
            m_GazeAngleSlider.interactable = isOn;
            m_HeadRotateSpeedSlider.interactable = isOn;
            AgentController.instance.needLookAtUser = isOn;
        });
        m_LookAtUserToggle.isOn = true;

        // m_GazeDistanceSlider
        m_PeripersonalSpaceSlider.minValue = 80;
        m_PeripersonalSpaceSlider.maxValue = 130;
        m_PeripersonalSpaceSlider.onValueChanged.AddListener((float value) =>
        {
            AgentController.instance.peripersonalSpace = value / 100.0f;        //"cm" to "m"
            m_PeripersonalSpaceText.text = ((int)value).ToString() + " cm";
        });
        m_PeripersonalSpaceSlider.value = 120;



        // m_GazeAngleSlider
        m_GazeAngleSlider.minValue = 160.0f;
        m_GazeAngleSlider.maxValue = 220.0f;
        m_GazeAngleSlider.onValueChanged.AddListener((float value) =>
        {
            AgentController.instance.angleOfGazeUser = value;
            m_GazeAngleText.text = value.ToString("0.0") + " °";
        });
        m_GazeAngleSlider.value = 200.0f;


        // m_GazeAngleSlider
        m_HeadRotateSpeedSlider.minValue = 0.1f;
        m_HeadRotateSpeedSlider.maxValue = 1.5f;
        m_HeadRotateSpeedSlider.onValueChanged.AddListener((float value) =>
        {
            AgentController.instance.headRotateSpeed = 1.0f / value;
            m_HeadRotateSpeedText.text = value.ToString("0.0") + " s";
        });
        m_HeadRotateSpeedSlider.value = 0.3f;

    }

    /// <summary>
    /// Show the name of model on UI
    /// </summary>
    /// <param name="idx"></param>
    public void SetCurrentModelNameOnUI(int idx)
    {
        m_CharacterModelText.text = m_CharacterModelDropdown.options[idx].text;
    }

    /// <summary>
    /// For button to open or close panel.
    /// </summary>
    public void SetPanelActive()
    {
        m_Panel.SetActive(!m_Panel.activeSelf);
        if (m_Panel.activeSelf)
        {
            m_ButtonText.text = "Close";
        }
        else
        {
            m_ButtonText.text = "Setting";
        }
    }


}
