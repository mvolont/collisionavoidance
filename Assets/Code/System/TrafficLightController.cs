﻿#pragma warning disable 0649 // Field is never assigned to, and will always have its default value
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Agent;

public class TrafficLightController : MonoBehaviour
{
    public static TrafficLightController instance;

    public float m_WaitTime;

    [SerializeField] private GameObject walkableMark1;
    [SerializeField] private GameObject walkableMark2;
    [SerializeField] private GameObject notWalkableMark1;
    [SerializeField] private GameObject notWalkableMark2;

    void Awake()
    {
        instance = this;
    }

    public IEnumerator SetTrafficLight()
    {
        SetWalkable(false);
        yield return new WaitForSeconds(m_WaitTime);
        SetWalkable(true);
    }

    private void SetWalkable(bool walkable)
    {
        walkableMark1.SetActive(walkable);
        walkableMark2.SetActive(walkable);
        notWalkableMark1.SetActive(!walkable);
        notWalkableMark2.SetActive(!walkable);
    }

}
