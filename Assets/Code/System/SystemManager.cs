﻿#pragma warning disable 0649 // Field is never assigned to, and will always have its default value
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Agent;

public class SystemManager : MonoBehaviour
{
    public bool EnableSystem;
    void Start()
    {
        StartCoroutine(SystemStart());
    }

    private IEnumerator SystemStart()
    {
        yield return new WaitUntil(() => EnableSystem);

        yield return StartCoroutine(CheckInit());
        AgentController.instance.LoadModelAndAnimationClip();
        ParameterSetting.instance.Init();

        AgentController.instance.StartAgentControl();
        
    }

    private IEnumerator CheckInit()
    {
        yield return new WaitUntil(() => ParameterSetting.instance != null);
        yield return new WaitUntil(() => TrafficLightController.instance != null);
        yield return new WaitUntil(() => AgentController.instance != null);
        yield return new WaitUntil(() => User.instance != null);
        yield return new WaitUntil(() => RecordManager.instance != null);
    }

}
