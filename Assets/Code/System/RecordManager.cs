﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecordManager : MonoBehaviour
{
    public static RecordManager instance;
    public float timeInterval = 0.1f;

    private List<Vector3> userTrajectory = new List<Vector3>();
    private float lateralDistance = 0;

    void Awake()
    {
        instance = this;
    }

    public void StartRecord()
    {
        StartCoroutine(UserTrajectory());
        StartCoroutine(LateralDistance());
    }

    public void StopRecord()
    {
        StopAllCoroutines();
    }


    private IEnumerator UserTrajectory()
    {
        userTrajectory.Clear();
        while (true)
        {
            Vector3 userPos = User.instance.transform.position;
            userPos.y = 0.3f;
            userTrajectory.Add(userPos);
            for (int i = 1; i < userTrajectory.Count; i++)
            {
                Debug.DrawLine(userTrajectory[i - 1], userTrajectory[i], Color.green, timeInterval);
            }
            yield return new WaitForSeconds(timeInterval);
        }
    }

    private IEnumerator LateralDistance()
    {
        bool IsCross = false;
        while (!IsCross)
        {
            Vector3 agentPos = Agent.AgentController.instance.Agent.transform.position;
            agentPos.y = 0;
            Vector3 userPos = User.instance.transform.position;
            userPos.y = 0;

            Vector3 direction = userPos - agentPos;
            if (Vector3.Dot(direction, Agent.AgentController.instance.Agent.transform.forward) <= 0)
            {
                lateralDistance = Vector3.Distance(agentPos, userPos);
                IsCross = true;
            }
            yield return null;
        }
        Debug.Log("[WeiChia] lateralDistance = " + lateralDistance);
    }

}
