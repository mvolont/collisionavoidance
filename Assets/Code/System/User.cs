﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class User : MonoBehaviour
{
    public static User instance;

    public Transform destination;

    //public GameObject userBody;
    //public GameObject userHead;

    private void Awake()
    {
        instance = this;
    }

    public bool ReachDestination()
    {
        Vector3 userPos = transform.position;
        userPos.y = 0;
        Vector3 destinationPos = destination.position;
        destinationPos.y = 0;
        return (userPos - destinationPos).sqrMagnitude < 0.01;
    }

}
