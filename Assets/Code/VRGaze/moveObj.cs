﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveObj : MonoBehaviour
{

    public float speed;
    // Update is called once per frame
    void Update()
    {

        transform.position = new Vector3(Mathf.PingPong(Time.time * speed, 2f), transform.position.y, transform.position.z);

        
    }
}
