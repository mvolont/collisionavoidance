﻿using UnityEngine;
using UnityEngine.UI;

public class UseMouse : MonoBehaviour
{
    public static Toggle m_Toggle;
    public static bool mouseToogleValue;


    void Start()
    {
        //Fetch the Toggle GameObject
        m_Toggle = GetComponent<Toggle>();

    }
    private void Update()
    {
        mouseToogleValue = m_Toggle.isOn;
    }
}