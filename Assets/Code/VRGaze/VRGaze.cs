﻿using System.Linq;
using UnityEngine;
using Tobii.XR;
using ViveSR.anipal.Eye;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using System;

public class VRGaze : MonoBehaviour
{
    public GameObject targetSphere;

    private bool gazeInput;
    private bool mouseInput;
    private bool angleInput;

    double[] coeff = {0.10714286f,  0.07142857f,  0.03571429f, 0f, -0.03571429f, -0.07142857f, -0.10714286f};

    Queue<double> xCoordinates = new Queue<double>();
    Queue<double> yCoordinates = new Queue<double>();
    Queue<double> degreesCoordinates = new Queue<double>();
    static double[] nArray;
    double[] xData = new double[7];
    double[] yData = new double[7];
    double[] degreeData = new double[7];

    double[] fixX = new double[11];
    double[] fixY = new double[11];
    double[] degreeFix = new double[11];

    public bool calculateFixations = true;
    private bool fixIncrement = false;

    public static List<int> totalFixations = new List<int>();
 
    public int fixations;
    float fixTime;

    float period;
    
    // dt is number of samples in velocity calculation (as many as filter len)
    float dt;
    float r;
    float dpi;
    float D;
    float degx;
    float degy;
    float velx;
    float vely;
    float velDegree;

    public static float gazeDistance;
    static public float vel;

    float screenWidth;
    float screenHeight;

    float screen;

    // setup HMD eye tracker.
    private FocusInfo FocusInfo;
    private readonly float MaxDistance = 50;
    private readonly GazeIndex[] GazePriority = new GazeIndex[] { GazeIndex.COMBINE, GazeIndex.LEFT, GazeIndex.RIGHT };
    private bool eye_callback_registered = false;

    private TobiiXR_EyeTrackingData _eyeTrackingDataLocal = new TobiiXR_EyeTrackingData();

    public Camera cam;
    //public GameObject gazeGO;

    private bool isPlayback;

    TobiiXR_GazeRay GazeRay;
    
    private Vector3 screenPos2;

    float maxRayDist = 15f;

    private Vector3 newCurrentPosition;


    Vector3 val;

    Queue<Vector3> angle = new Queue<Vector3>();

    Vector3[] sData = new Vector3[1];
    float[] sData2 = new float[7];

    float finalAngle;


    SRanipal_Eye_Framework SRanipal;
    SRanipal_Eye_Framework.SupportedEyeVersion eyeVersion;

    public static string gazeIntersection;


    private void Awake(){


        fixations = 0;

        gazeInput = true;
        //gazeInput  = UseGaze.GazeToogleValue;
        mouseInput = UseMouse.mouseToogleValue;

        targetSphere.transform.position = new Vector3(0f, 0f,0f);
        
        /*
        The eye tracker rate is 120 Hz, meaning that that's how fast the cameras
        looking at the eyes work, 120 times per second.  So the period between
        samples would be 1/120 = 0.00833333 seconds or 8.33333 ms.
        */
        
        // 8.333f ms sampling period, assuming 120hz sampling rate
        period = 8.333f;

        //checking if vive pro eye data is enabled
        if (!SRanipal_Eye_Framework.Instance.EnableEye){
        enabled = false;
        return;
        } 

        //hmd screen dimensions
        screenWidth =  2880.0f;
        screenHeight = 1600.0f;

        // HMD display diagonal length in inches
        screen = 7.0f;

        // distance from eyes to HMD display (inches)
        D = 3f;
        
    }

    // Update is called once per frame
    void Update(){



        if (SRanipal_Eye_Framework.Status != SRanipal_Eye_Framework.FrameworkStatus.WORKING &&
                SRanipal_Eye_Framework.Status != SRanipal_Eye_Framework.FrameworkStatus.NOT_SUPPORT) return;
        _eyeTrackingDataLocal.GazeRay.IsValid = SRanipal_Eye.GetGazeRay(GazeIndex.COMBINE, out _eyeTrackingDataLocal.GazeRay.Origin, out _eyeTrackingDataLocal.GazeRay.Direction);






        foreach (GazeIndex index in GazePriority)
        {
            Ray GazeRay;
            bool eye_focus;
            eye_focus = SRanipal_Eye_v2.Focus(index, out GazeRay, out FocusInfo, 0, MaxDistance);

            //Vector3 forward = transform.TransformDirection(Vector3.forward) * 200;
            //Debug.DrawRay(GazeRay.origin, FocusInfo.point, Color.green);

            if (eye_focus)
            {
                targetSphere.transform.position = new Vector3(FocusInfo.point.x, FocusInfo.point.y, FocusInfo.point.z);
            }

            Ray GazeIntersect = new Ray(GazeRay.origin, FocusInfo.point);
            RaycastHit hit;
            if (Physics.Raycast(GazeIntersect, out hit))
            {
                if (hit.collider != null)
                {
                    gazeIntersection = hit.transform.name;
                    gazeDistance = hit.distance;
                    //Debug.Log(gazeDistance);
                }
            }
        }

        val = targetSphere.transform.position;

        //add gaze vector to a deque of 2 elemtents so I can obtain current vs previos vector angle
        angle.Enqueue(val);

        while (angle.Count > 2)
        {
            angle.Dequeue();
        }

        sData = angle.ToArray();

        try
        {
            //compute angle difference between vectors
            finalAngle = Vector3.Angle(sData[1], sData[0]);
        }
        catch { }
        //Debug.Log("finalAngle "+ finalAngle);
        //adding angle difference to a deque
        degreesCoordinates.Enqueue(finalAngle);

        while (degreesCoordinates.Count > 7)
        {
            degreesCoordinates.Dequeue();
        }

        degreeData = degreesCoordinates.ToArray();
        degreeFix = Convolve(coeff, degreeData);


        //converting pixels to inches
        r = Mathf.Sqrt(screenWidth * screenWidth + screenHeight * screenHeight);
        dpi = r / screen;

        // dt is how much time used in convolution calculation:
        // period in seconds * number of samples (as many as filter len)
        dt = (period / 1000.0f) * 7.0f;

        //velocity calculation (angle over time)
        vel = Convert.ToSingle(degreeFix[6]) / dt;

        //Velocity.text = string.Format("{0}", Mathf.Abs(vel));

        //if we started recording values, categorize gaze speed
        if (ScenarioManager.recordingStarted) { 
            //fixIncrement: only allow to increment fixations after velocity limit
            if (Mathf.Abs(vel) < 18 && !fixIncrement)
            {
                //This boolean is for initialization. 
                //fixation is true so let's append them to a list
                fixIncrement = true;
                fixations += 1;
                totalFixations.Add(fixations);

                Debug.Log("count: "+totalFixations.Count);
                Debug.Log("Fixation " + vel);

            }

            if (Mathf.Abs(vel) > 18)
            {
                fixIncrement = false;
                Debug.Log("not Fixation " + vel);
                //totalFixations.Add(0);

            }
        }
    }
    private static double[] Convolve(double[] u, double[] v){
        double[] revisedArray = Enumerable.Reverse(v).ToArray();
        int sizeV = (v.Length) - 1;

        try
        {
            nArray = new double[sizeV];
        }
        catch { }
        for (int i = 0; i < sizeV; i++){
            nArray[i] = 0;

        }

        double[] paddingArrayU = ConcatArrays(nArray, u, nArray);

        int m = u.Length;
        int n = v.Length;
        int k = m + n - 1;

        double[] w = new double[k];

        for (int i = 0; i < k; i++)
        {
            w[i] = 0;
        }

        for (int i = 0; i < k; i++)
        {
            for (int j = 0; j < n; j++)
            {
                w[i] += paddingArrayU[i + j] * revisedArray[j];
            }
        }
        return w;
    }

    public static T[] ConcatArrays<T>(params T[][] list)
    {
        var result = new T[list.Sum(a => a.Length)];
        int offset = 0;
        for (int x = 0; x < list.Length; x++)
        {
            list[x].CopyTo(result, offset);
            offset += list[x].Length;
        }
        return result;
    }

}
