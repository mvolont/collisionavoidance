﻿using UnityEngine;
using UnityEngine.UI;

public class UseGaze : MonoBehaviour
{
    public static Toggle m_Toggle;
    public static bool GazeToogleValue;

    void Start()
    {
        //Fetch the Toggle GameObject
        m_Toggle = GetComponent<Toggle>();

    }
    private void Update()
    {
        GazeToogleValue = m_Toggle.isOn;
    }


}