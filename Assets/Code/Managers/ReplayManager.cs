﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine.UI;

public class ReplayManager : MonoBehaviour {

    GazeList gpList;
    public int curIndex;

    [SerializeField]
    private int pid = 0;
    public int PID { get { return pid; } set { pid = value; } }

    string outputDataDirectoryName = "OutputData";
    string eyetrackingInfoDirectoryName = "EyetrackingData";

    GazeInfo CurGPI { get { return gpList.GazeInfoList[curIndex]; } }

    float maxRayDist = 15f;

    [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
    public static extern void mouse_event(long dwFlags, long dx, long dy, long cButtons, long dwExtraInfo);
    private const int MOUSEEVENTF_LEFTDOWN = 0x02;
    private const int MOUSEEVENTF_LEFTUP = 0x04;
    private const int MOUSEEVENTF_MOVE = 0x0001;


    [DllImport("user32.dll", EntryPoint = "SetCursorPos")]
    public static extern void SetCursorPos(int X, int Y);


    [DllImport("user32.dll", SetLastError = true)]
    static extern void keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo);

    public const int KEYEVENTF_EXTENDEDKEY = 0x0001;
    public const int KEYEVENTF_KEYUP = 0x0002;
    public const int VK_LCONTROL = 0xA2;
    public const int D = 0x44;
    public const int C = 0x43;
    public const int T = 0x54;
    public const int L = 0x4C;


    int mouseX;
    int mouseY;

    public static int fixationValue;
    public static float velocityValue;
    public static string gazingAt;
    public static float gazingDistance;

    public string RootPath
    {
        get
        {
            return Directory.GetParent(Application.dataPath).FullName;
        }
    }

    public string OutputDataDirectory
    {
        get
        {
            return RootPath + "/" + outputDataDirectoryName;
        }
    }

    void OnEnable()
    {
        EventManager.Instance.AddListener<StartPlaybackEvent>(HandleStartPlayback);
    }

    void OnDisable()
    {
        if (EventManager.Instance)
        {
            EventManager.Instance.RemoveListener<StartPlaybackEvent>(HandleStartPlayback);
        }
    }

    private void HandleStartPlayback(StartPlaybackEvent e)
    {
        Load();
    }

    void Load()
    {
        string fullPath = OutputDataDirectory + "/" + eyetrackingInfoDirectoryName + "/Gazeinfo" + PID.ToString() + ".xml";
        Debug.Log(fullPath);
        gpList = new GazeList();
        MyXML.GetObject(ref gpList, fullPath);

        StartCoroutine(PlayCoroutine());
    }

    public void DoMouseClick()
    {
        mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
        mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);

    }

    IEnumerator PlayCoroutine()
    {
        while (curIndex < gpList.GazeInfoList.Count - 1 && curIndex >= 0)
        {
            float baseTimeUnit = CurGPI.TimeInfo - GetPrevTime();

            int mouseClick = CurGPI.MouseInfo;
            
            TransformInfo curGaze = CurGPI.GazedInfo;
            TransformInfo curCam = CurGPI.CamInfo;

            EventManager.Instance.TriggerEvent(new SetTransformPositionEvent(curGaze.Name, new Vector3(curGaze.Pos.X, curGaze.Pos.Y, curGaze.Pos.Z), new Quaternion(curGaze.Rot.X, curGaze.Rot.Y, curGaze.Rot.Z, curGaze.Rot.W)));
            EventManager.Instance.TriggerEvent(new SetTransformPositionEvent(curCam.Name, new Vector3(curCam.Pos.X, curCam.Pos.Y, curCam.Pos.Z), new Quaternion(curCam.Rot.X, curCam.Rot.Y, curCam.Rot.Z, curCam.Rot.W)));

            EventManager.Instance.TriggerEvent(new BroadcastCurPlaybackEntryEvent(CurGPI));
            fixationValue = (int)CurGPI.FixationInfo;
            velocityValue = (int)CurGPI.VelocityInfo;
            gazingAt      = (string)CurGPI.GazeIntersectInfo;
            gazingDistance = (float)CurGPI.GazeDistanceInfo;


            //Debug.Log(fixationValue);

            //VRGaze.labelFixations.text = string.Format("{0:00}", fixationValue);



            if (baseTimeUnit > 1f)
            {
                baseTimeUnit = 0f;
            }

            //MOUSE MOVEMENT DEACTIVATED
            //mouseX = (int)CurGPI.MousePosInfo.X;
            //mouseY = Screen.height - (int)CurGPI.MousePosInfo.Y;
            //SetCursorPos(mouseX, mouseY);




            yield return new WaitForSeconds(baseTimeUnit);
            curIndex++;
        }
    }

    float GetPrevTime()
    {
        if (curIndex < 1 || curIndex > gpList.GazeInfoList.Count - 1)
        {
            return CurGPI.TimeInfo;
        }
        else
        {
            return gpList.GazeInfoList[curIndex - 1].TimeInfo;
        }
    }

    public void UpdatePID(string pidString)
    {
        int.TryParse(pidString, out pid);
    }
}
