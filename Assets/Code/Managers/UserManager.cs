﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System;


public class UserManager : Singleton<UserManager> {

    // Properties
    [SerializeField]
    private int pid = 0;
    public int PID { get { return pid; } set { pid = value; } }


    //private float xmlRefreshRate = 1f / 30f; //30FPS

    string outputDataDirectoryName = "OutputData";
    string eyetrackingInfoDirectoryName = "EyetrackingData";


    public string RootPath
    {
        get
        {
            return Directory.GetParent(Application.dataPath).FullName;
        }
    }

    public string OutputDataDirectory
    {
        get
        {
            return RootPath + "/" + outputDataDirectoryName;
        }
    }


    void OnEnable()
    {
        EventManager.Instance.AddListener<SendRecordedGPListEvent>(HandleSendRecord);
    }

    private void HandleSendRecord(SendRecordedGPListEvent e)
    {
        MyXML.SaveObject(e.gpList, OutputDataDirectory, eyetrackingInfoDirectoryName, PID.ToString(), "GazeInfo");
    }

    void OnDisable()
    {
        if (EventManager.Instance)
        {
            EventManager.Instance.RemoveListener<SendRecordedGPListEvent>(HandleSendRecord);
        }
    }

    public void UpdatePID(string pidString)
    {
        int.TryParse(pidString, out pid);
    }


    public static void CreateDirectory(string path)
    {
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
    }


}


