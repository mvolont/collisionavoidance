﻿using UnityEngine;
using System.Collections;
using System.Net.Sockets;
using System.Text;
using System.IO;
using System.Xml.Linq;
using System.Collections.Generic;
using System;

public class GazepointManager : MonoBehaviour {


    int readBufferSize = 1024;
    TcpClient client;
    byte[] readBuffer = new byte[1024];
    public string strMessage = string.Empty;
    NetworkStream myNetworkStream;

    public string recMessage;
    public string curServerMessage;

    Dictionary<string, GPCommand> commandDict = new Dictionary<string, GPCommand>();

    public bool isInitialized = true;


    void Update()
    {

        if (commandDict.Count > 0) EventManager.Instance.TriggerEvent(new BroadcastGazeCommandEvent(commandDict));

    }

    void OnEnable()
    {
        EventManager.Instance.AddListener<SetupGazepointTrackerEvent>(HandleSetupTracker);
    }

    void OnDisable()
    {
        if (EventManager.Instance)
        {
            EventManager.Instance.RemoveListener<SetupGazepointTrackerEvent>(HandleSetupTracker);
        }
    }

    private void HandleSetupTracker(SetupGazepointTrackerEvent e)
    {
        SetupTracker();
    }

    public void SetupTracker()
    {
         AddCommand("FIRST_VALUE", new string[1] { "USER" });
    }


    IEnumerator InitBranch()
    {
        yield return StartCoroutine(GetValues());
    }


    IEnumerator GetValues()
    {
        strMessage = string.Empty;

        foreach (KeyValuePair<string, GPCommand> i in commandDict)
        {
            string prevCommand = strMessage;
            //ProcessCommand(i.Value.GetCommand());

            while (prevCommand == strMessage)
            {
                yield return null;
            }

            i.Value.Poll(strMessage);
        }
    }

   
    void PollRecords(string rec)
    {
        foreach(KeyValuePair<string, GPCommand> i in commandDict)
        {
            i.Value.PollRecord(rec);
        }
    }


    void OnApplicationQuit()
    {
        if (isInitialized)
        {
            if (client != null) client.Close();
        }

    }

    void AddCommand(string name, string[] attribs)
    {
        GPCommand cmd = new GPCommand(name, attribs);
        commandDict.Add(cmd.name, cmd);
    }
    
}

public class GPCommand{

    public string name;
    public Dictionary<string, string> attributes = new Dictionary<string, string>();

    public GPCommand(string name)
    {
        this.name = name;
    }

    public GPCommand(string name, string[] attribs)
    {

        this.name = name;
        foreach(string i in attribs)
        {
            attributes.Add(i, "NULL");
        }
        attributes.Add("ID", name);
    }
 
    public void Poll(string serverReturn)
    {
        XElement xmlTree = XElement.Parse(serverReturn);
        IEnumerable<XAttribute> attribs = xmlTree.Attributes();

        foreach(XAttribute i in attribs)
        {
            if(i.Name == "ID" && i.Value == name)
            {
                foreach(XAttribute j in attribs)
                {
                    if (attributes.ContainsKey(j.Name.ToString()))
                    {
                        attributes[j.Name.ToString()] = j.Value.ToString();
                    }
                }
            }
        }
     
    }

    public void PollRecord(string serverReturn)
    {
        XElement xmlTree = XElement.Parse(serverReturn);
        IEnumerable<XAttribute> attribs = xmlTree.Attributes();

        foreach(XAttribute i in attribs)
        {
            if (attributes.ContainsKey(i.Name.ToString()))
            {
                attributes[i.Name.ToString()] = i.Value.ToString();
            }
        }
    }

}
