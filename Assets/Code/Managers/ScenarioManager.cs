﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ScenarioManager : MonoBehaviour {

    Animator myAnim;

    public GameObject fixMan;
    public GameObject GazePanel;
    public GameObject Camerarig;
    public GameObject playbackCam;
    [HideInInspector]
    static public bool recordingStarted = false;
    static public bool replayingStarted = false;


    // Use this for initialization
    void Awake () {
        myAnim = GetComponent<Animator>();
        playbackCam.SetActive(false);
    }

    
    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(KeyCode.Escape)){
            myAnim.SetTrigger("Main");
        }
    }

    public void SetStateButton(string state){
        myAnim.SetBool(state, true);
        if (state == "Playback") {

            fixMan.SetActive(false);
            Camerarig.SetActive(false);
            playbackCam.SetActive(true);
            replayingStarted = true;

        }
        if (state == "Recording")
        {
            fixMan.SetActive(true);
            recordingStarted = true;
        }
    }

}
